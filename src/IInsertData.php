<?php

namespace Duna\Console;


use Doctrine\ORM\EntityManagerInterface;

interface IInsertData
{
    public function insert(EntityManagerInterface $em);
}