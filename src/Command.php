<?php

namespace Duna\Console;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Tools\SchemaTool;
use Duna\Plugin\Manager\Facade;
use Duna\Plugin\Manager\IPlugin;
use Nette\SmartObject;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Command
 *
 * @package Duna\Console
 *
 * @property-read \Doctrine\ORM\EntityManagerInterface $em
 * @property-read \Symfony\Component\Console\Input\InputInterface $input
 * @property-read \Symfony\Component\Console\Output\OutputInterface $output
 * @property-read \Doctrine\ORM\Tools\SchemaTool $schemaTool
 * @property-read \Kdyby\Doctrine\Mapping\ClassMetadata[] $metadatas
 */
abstract class Command extends \Symfony\Component\Console\Command\Command
{
    use SmartObject;

    /** @var  \Doctrine\ORM\EntityManagerInterface */
    private $em;
    /** @var  \Symfony\Component\Console\Input\InputInterface */
    private $input;
    /** @var  \Symfony\Component\Console\Output\OutputInterface */
    private $output;
    /** @var  \Kdyby\Doctrine\Mapping\ClassMetadata[] */
    private $metadatas = [];
    /** @var  \Doctrine\ORM\Tools\SchemaTool */
    private $schemaTool;

    public function addMessage($messeger, $newline = true)
    {
        $this->output->write('<fg=white;bg=black>' . $messeger . '</>', $newline);
    }

    public function addMessageComment($message, $newline = true)
    {
        $this->output->write('<comment>' . $message . '</comment>', $newline);
    }

    public function addMessageError($messeger, $newline = true)
    {
        $this->output->write('<fg=white;bg=red>' . $messeger . '</>', $newline);
    }

    public function addMessageInfo($messeger, $newline = true)
    {
        $this->output->write('<info>' . $messeger . '</info>', $newline);
    }

    public function addMessageSuccess($messeger, $newline = true)
    {
        $this->output->write('<fg=white;bg=green>' . $messeger . '</>', $newline);
    }

    public function checkInstalled()
    {
        $facade = new Facade($this->em);
        $reflection = new \ReflectionClass($this->getExtension());
        if ($reflection->implementsInterface(IPlugin::class)) {
            $class = $this->getExtension();
            $entity = $class::getPluginInfo()['plugin'];
            if ($facade->getOneByHash(md5($entity->name))) {
                return 0;
            }
        }
        return 1;
    }

    public function getData()
    {
        return [];
    }

    /**
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    public function getEm()
    {
        return $this->em;
    }

    public function getEntityMappings($onlyKey = false, $data = [])
    {
        return $onlyKey ? array_keys($data) : $data;
    }

    public function getFixturesPath()
    {
        return '';
    }

    /**
     * @return \Symfony\Component\Console\Input\InputInterface
     */
    public function getInput()
    {
        return $this->input;
    }

    public function getJoinTableName($em, $assoc, $class, $platform)
    {
        if (isset($assoc['joinTable']['schema']) && !method_exists($class, 'getSchemaName')) {
            return $assoc['joinTable']['schema'] . '.' . $em->getConfiguration()->getQuoteStrategy()->getJoinTableName($assoc, $class, $platform);
        }

        return $em->getConfiguration()->getQuoteStrategy()->getJoinTableName($assoc, $class, $platform);
    }

    public function getMetadataTables($onlyMain = false, $joinTable = false)
    {
        $tables = [];
        $excludedMetadata = [];
        foreach ($this->metadatas as $metadata) {
            /** @var $metadata \Kdyby\Doctrine\Mapping\ClassMetadata */
            if (!in_array($metadata->namespace, $this->getEntityMappings(true)))
                $excludedMetadata[$metadata->getName()] = $metadata;
            else
                $tables[$metadata->getName()] = $metadata;
        }

        if ($onlyMain)
            return $tables;

        $result = [];
        if ($joinTable === false) {
            foreach ($tables as $class) {
                /** @var \Kdyby\Doctrine\Mapping\ClassMetadata $class */
                $result[$class->getName()] = $class;
                foreach ($class->associationMappings as $assoc) {
                    if ($assoc['isOwningSide']) {
                        if (isset($assoc['targetEntity']) && !array_key_exists($assoc['targetEntity'], $tables)) {
                            $result[$assoc['targetEntity']] = $excludedMetadata[$assoc['targetEntity']];
                        }
                    }
                }
            }
        } else {
            $platform = $this->em->getConnection()->getDatabasePlatform();
            foreach ($excludedMetadata as $metadata) {
                $result[] = $metadata->table['name'];
                foreach ($metadata->associationMappings as $assoc) {
                    if ($assoc['isOwningSide'] && $assoc['type'] == ClassMetadata::MANY_TO_MANY) {
                        $result[] = $this->getJoinTableName($this->em, $assoc, $metadata, $platform);
                    }
                }
            }
        }
        return $result;

    }

    /**
     * @return \Kdyby\Doctrine\Mapping\ClassMetadata[]
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * @return \Symfony\Component\Console\Output\OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @return \Doctrine\ORM\Tools\SchemaTool
     */
    public function getSchemaTool()
    {
        return $this->schemaTool;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emHelper = $this->getHelper('em');
        $this->em = $emHelper->getEntityManager();

        $this->input = $input;
        $this->output = $output;
        $this->metadatas = $this->em->getMetadataFactory()->getAllMetadata();
        $this->schemaTool = new SchemaTool($this->em);

        $this->addMessage('COMMAND -> ' . $this->getTitle());


        if ($this->checkInstalled() === 0) {
            $this->addMessageInfo('Already installed.');
            return 0;
        }

        $this->addMessage(str_repeat('=', strlen($this->getTitle()) + 15));
        $this->addMessage('');

        if (count($this->getDependentCommands())) {
            $this->addMessage('Install dependent commands');
            foreach ($this->getDependentCommands() as $dependentCommand) {
                $newInput = new ArrayInput($dependentCommand);
                $command = $this->getApplication()->find($dependentCommand['command']);

                if ($command->run($newInput, $this->output) !== 0) {
                    return 1;
                }
            }
        }

        $this->addMessage('RUN COMMAND ' . $this->getName());
        if ($this->runCommand() === 0) {
            $this->addMessageSuccess('SUCCESS');
        } else {
            $this->addMessageError('FAIL');
            return 1;
        }

        if (strlen($this->getFixturesPath()) > 0) {
            $this->addMessage('');
            $this->addMessage('INSTALL FIXTURES');
            try {
                $excluded = $this->getMetadataTables(false, true);
                $loader = new Loader();
                $loader->loadFromDirectory($this->getFixturesPath());
                $fixtures = $loader->getFixtures();

                $purger = new ORMPurger($this->em, $excluded);
                $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
                $purger->purge();

                $executor = new ORMExecutor($this->em, $purger);
                $executor->setLogger(function ($message) use ($output) {
                    $output->writeln(sprintf('<comment>></comment> <info>%s</info>', $message));
                    \Tracy\Debugger::log(sprintf('Fixture: %s', $message), \Tracy\ILogger::DEBUG);
                });
                $executor->execute($fixtures, true);
                $this->output->writeln('<fg=black;bg=yellow>You should loggout from application and delete cache.');
                return 0;
            } catch (\Exception $ex) {
                $this->addMessageError("{$ex->getMessage()}");
                \Tracy\Debugger::log($ex, \Tracy\ILogger::ERROR);
                return 1;
            }
        }

        if (count($this->getData())) {
            $this->addMessage("INSERT DATA", true);
            foreach ($this->getData() as $sourceClass) {
                $ref = new \ReflectionClass($sourceClass);
                if ($ref->isInstantiable() && $ref->implementsInterface(IInsertData::class)) {
                    $this->addMessageComment('    > ', false);
                    $this->addMessageInfo($sourceClass, false);
                    $class = new $sourceClass;
                    $class->insert($this->em);
                    $this->addMessageSuccess('OK');
                }
            }
        }
    }

    abstract public function getTitle();

    abstract public function runCommand();

    abstract public function getDependentCommands();

    abstract public function getExtension();
}