<?php

namespace Duna\Console;

interface IEntityConsoleProvider
{
    public static function entityMappings();
}