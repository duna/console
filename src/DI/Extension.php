<?php

namespace Duna\Console\DI;

use App;
use Nette\DI\CompilerExtension;

class Extension extends CompilerExtension
{
    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->loadFromFile(__DIR__ . '/config.neon');
        $this->compiler->parseServices($builder, $config);
    }
}
