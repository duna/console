<?php

namespace Duna\Console;

use Kdyby\Console\DI\ConsoleExtension;
use Nette\DI\CompilerExtension;
use Nette\StaticClass;

class Helper
{
    use StaticClass;

    public static function loadCommands(CompilerExtension $compiler, array $commands)
    {
        $builder = $compiler->getContainerBuilder();
        foreach ($commands as $i => $command) {
            $builder->addDefinition($compiler->prefix('cli.duna.' . $i))
                ->addTag(ConsoleExtension::TAG_COMMAND)
                ->setInject(false)
                ->setClass($command);
        }
    }
}